import { snackbar } from '@ifood/pomodoro-components';
import { useEffect, useState } from 'react';

type Post = {
  id: number;
  userId: number;
  title: string;
  body: string;
};

const getPosts = () => fetch('https://jsonplaceholder.typicode.com/posts');

export const usePosts = () => {
  const [posts, setPosts] = useState<Post[]>([]);

  useEffect(() => {
    getPosts()
      .then(async (res) => {
        const data: Post[] = await res.json();
        setPosts(data);
      })
      .catch(() =>
        snackbar({
          timeout: 2000,
          autoClose: true,
          variant: 'error',
          message: 'Unexpected error',
        })
      );
  }, []);

  return { posts };
};
