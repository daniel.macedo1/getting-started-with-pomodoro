import { Heading1, Page } from '@ifood/pomodoro-components';
import { Outlet, useLocation } from 'react-router-dom';
import { Link } from '../../components/Link';

const links = [
  { title: 'Home', linkProps: { to: '/' } },
  { title: 'Posts', linkProps: { to: '/posts' } },
];

export const MainLayout = () => {
  const location = useLocation();
  const currentLink = links.find(
    ({ linkProps }) => linkProps.to === location.pathname
  );

  return (
    <Page>
      <Page.Header>
        <Page.Breadcrumb
          linkComponent={Link}
          linkList={links}
          currentLinkTitle={currentLink?.title}
        />
        <Heading1>{currentLink?.title}</Heading1>
      </Page.Header>
      <Page.Body>
        <Outlet />
      </Page.Body>
    </Page>
  );
};
