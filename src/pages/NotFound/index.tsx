import { Button, EmptyState } from "@ifood/pomodoro-components";
import { useNavigate } from "react-router-dom";

export const NotFound = () => {
  const navigate = useNavigate()

  const goToHome = () => navigate("/")

  return (
    <EmptyState
      title="Page not found"
      description='This url does not exists'
      imgSrc="https://via.placeholder.com/100"
      imgAlt="Alt icon"
    >
      <Button variant="primary" mr={20} mb={20} onClick={goToHome}>
        Home
      </Button>
    </EmptyState>
    )
};