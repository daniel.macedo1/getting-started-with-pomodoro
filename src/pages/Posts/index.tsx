import { List } from '@ifood/pomodoro-components';
import { usePosts } from '../../hooks/usePosts';

export const Posts = () => {
  const { posts } = usePosts();

  return (
    <List>
      <List.Header>My list of posts</List.Header>
      {posts.map((post) => (
        <List.Item key={post.id}>{post.title}</List.Item>
      ))}
    </List>
  );
};
