import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { NotFound } from './pages/NotFound';
import { Posts } from './pages/Posts';
import { MainLayout } from './components/MainLayout';
import { Home } from './pages/Home';

const router = createBrowserRouter([
  {
    path: '/',
    element: <MainLayout />,
    children: [
      {
        index: true,
        element: <Home />,
      },
      {
        path: 'posts',
        element: <Posts />,
      },
    ],
  },
  { path: '*', element: <NotFound /> },
]);

export const App = () => <RouterProvider router={router} />;
