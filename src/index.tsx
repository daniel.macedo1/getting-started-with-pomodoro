import React from 'react';
import ReactDOM from 'react-dom/client';
import { PomodoroProvider } from '@ifood/pomodoro-components';

import { App } from './App';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <React.StrictMode>
    <PomodoroProvider>
      <App />
    </PomodoroProvider>
  </React.StrictMode>
);
